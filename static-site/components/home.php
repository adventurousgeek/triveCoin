<section class="page-header text-white truth-discovered">
    <canvas></canvas>
    <canvas></canvas>
    <div class="container header-container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <h1>Truth Discovered  with Trive™</h1>
                <p>FAKE NEWS IS A PROBLEM</p>
            </div>
            <div class="col-md-6 col-xs-12">
                <a href="" class="btn btn-download btn-orange btn-circle"><svg class="icon-chrome"><use xlink:href="#icon-chrome"></use></svg>Download Plugin</a>
            </div>
        </div>
    </div>
    <div class="container header-container">
        <ul class="latest-news list-unstyled list-inline">
            <li class="latest-news-item text-center">
                <a href="">
                    <div class="news-thumbnail bg-center bg-cover lazy" data-original="assets/images/post-item.jpg"></div>
                    <div class="news-meta">
                        <h2>Vitalik is not dead.</h2>
                        <p>Hoax Over 'Dead' Ethereum Founder Spurs $4 Billion Wipe Out</p>
                    </div>
                </a>

            </li>

            <li class="latest-news-item text-center">
                <a href="">
                    <div class="news-thumbnail bg-center bg-cover lazy" data-original="assets/images/post-item.jpg"></div>
                    <div class="news-meta">
                        <h2>Vitalik is not dead.</h2>
                        <p>Hoax Over 'Dead' Ethereum Founder Spurs $4 Billion Wipe Out</p>
                    </div>
                </a>

            </li>

            <li class="latest-news-item text-center">
                <a href="">
                    <div class="news-thumbnail bg-center bg-cover lazy" data-original="assets/images/post-item.jpg"></div>
                    <div class="news-meta">
                        <h2>Vitalik is not dead.</h2>
                        <p>Hoax Over 'Dead' Ethereum Founder Spurs $4 Billion Wipe Out</p>
                    </div>
                </a>

            </li>

            <li class="latest-news-item text-center">
                <a href="">
                    <div class="news-thumbnail bg-center bg-cover lazy" data-original="assets/images/post-item.jpg"></div>
                    <div class="news-meta">
                        <h2>Vitalik is not dead.</h2>
                        <p>Hoax Over 'Dead' Ethereum Founder Spurs $4 Billion Wipe Out</p>
                    </div>
                </a>

            </li>
            <li class="latest-news-item text-center">
                <a href="">
                    <div class="news-thumbnail bg-center bg-cover lazy" data-original="assets/images/post-item.jpg"></div>
                    <div class="news-meta">
                        <h2>Vitalik is not dead.</h2>
                        <p>Hoax Over 'Dead' Ethereum Founder Spurs $4 Billion Wipe Out</p>
                    </div>
                </a>

            </li>


        </ul>
    </div>
</section>

<section class="countdown-to-crowdsale">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-3">
                <h1>Countdown to Crowdsale</h1>
            </div>
            <div class="col-md-4">
                <div class="count-down-clock"></div>
            </div>
            <div class="col-md-3">
                <div class="text-center">
                    <a href="#" class="btn btn-orange btn-circle">Join Presale</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how-we-help">
    <div class="container"> 
        <div class="row">
            <div class="col-md-12">
                <div class="intro-text text-dark-gray text-center">
                    <h1>How we help you find the truth</h1>
                    <p>Trive is a social science global consensus engine that researches and clarifies<br>truth through human swarmed crowd wisdom.</p>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="trive-features bg-contain bg-center lazy" data-original="assets/images/trive-features.jpg">
                    <div class="row">

                        <div class="col-md-offset-6 col-md-6">
                            <h1>Trive Plugin</h1>
                            <p>A browser extension that alerts users to unreliable news sources.</p>
                            <p>The browser extension icon will indicate current websites's score Sites with very low score will trigger a notification.</p>
                            <a href="#">Learn More <svg class="icon icon-chevron-with-circle-right"><use xlink:href="#icon-chevron-with-circle-right"></use></svg></a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h1>Trive Engine</h1>
                            <p>The engine creates a Nash Equlibrium incentive structure for the research into news story claims.</p>
                            <p>An antagonistic relationship between researchers and verifiers creates a checks/balances process that reinforcces research quality.</p>
                            <a href="#">Learn More <svg class="icon icon-chevron-with-circle-right"><use xlink:href="#icon-chevron-with-circle-right"></use></svg></a>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-offset-6 col-md-6">
                            <h1>Trive Participants</h1>
                            <p>Trive is a social games global consensus engine that researches and clarifies facts through Human Swarmed crowd wisdom.</p>
                            <p>We incent people to do primary into news stories, compensate them with our own coin, TriveCoin, and hash/stash the results on the Blockchain.</p>
                            <a href="#">Learn More <svg class="icon icon-chevron-with-circle-right"><use xlink:href="#icon-chevron-with-circle-right"></use></svg></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>


<section class="cogs-behind text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Cogs behind <strong>Trive&trade;</strong></h1>
                <h2>Executive & Advisory Team</h2>
            </div>
        </div>

        <?php
        $team_members = array('1','2','3','4','5','6','7','8','9','10','11');
        $team_member_first_row = '';
        $team_member_second_row = '';
        $index = 0;
        foreach($team_members as $team_member):
            if($index<5):
                $team_member_first_row .= '<div class="col-sm-2">
                                                <div class="team-member-container">
                                                    <a href="#"><svg class="icon icon-linkedin-with-circle"><use xlink:href="#icon-linkedin-with-circle"></use></svg></a>

                                                    <div class="team-member horizontal-center bg-center bg-cover lazy" data-original="assets/images/team/'.$team_member.'.jpg" >

                                                        <div class="team-member-meta">

                                                            <h4>David Mondrus</h4>
                                                            <p>CEO</p>
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>';
            else:
                $team_member_second_row .= '<div class="col-sm-2">
                                                <div class="team-member-container">
                                                   <a href="#"><svg class="icon icon-linkedin-with-circle"><use xlink:href="#icon-linkedin-with-circle"></use></svg></a>

                                                    <div class="team-member horizontal-center bg-center bg-cover lazy" data-original="assets/images/team/'.$team_member.'.jpg">
                                                        <div class="team-member-meta">
                                                            <h4>David Mondrus</h4>
                                                            <p>CEO</p>
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>';
            endif;
            $index++;
        endforeach; ?>

        <div class="row">
            <div class="team executive-team">
                <div class="col-md-offset-1 col-md-10">
                    <div class="row ten-columns">
                        <?php echo $team_member_first_row; ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <?php echo $team_member_second_row; ?>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Operations Team</h2>
            </div>
            <div class="col-md-12">
                <div class="team operations-team">
                    <div class="row nine-columns">
                        <?php echo $team_member_second_row; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="inital-coin-offering bg-orange text-white lazy bg-center" data-original="assets/images/ico-bg.png">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-block text-center">
                    <h1>Initial Coin Offering <span>(ICO)</span></h1>

                    <p>ICO is an alternative method of investing raising in the form of "crypto-shares".</p>

                    <p>Each crypto-share is presented in a block-chain token from based on "smart" contracts.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="countdown-to-crowdsale">
                <div class="col-md-offset-1 col-md-3">
                    <h1>Countdown to Crowdsale</h1>
                </div>
                <div class="col-md-4">
                    <div class="count-down-clock text-white"></div>
                </div>
                <div class="col-md-4">
                    <div class="text-center">
                        <a href="#" class="btn btn-white btn-block btn-circle">Join Presale Now</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="ico-timeline bg-dark-orange text-white">
    <span>ICO TIMELINE</span>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="horizontal-timeline hidden-xs hidden-sm">
                    <ul class="list-unstyled list-inline list-btc">
                        <li>0 BTC</li>
                        <li>500 BTC</li>
                        <li>750 BTC</li>
                        <li>1000 BTC</li>
                        <li>1250 BTC</li>
                        <li>1500 BTC</li>
                    </ul>

                    <img src="assets/images/timeline-bar.png" class="img-responsive horizontal-center img-timeline">

                    <ul class="list-unstyled list-inline list-btc list-bonus">
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                        </li>
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                        </li>
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                        </li>
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                        </li>
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                        </li>

                    </ul>
                </div>

            </div>
            <div class="timeline-vertical hidden-md- hidden-lg">

                <div class="col-md-12">
                    <div class="row">
                            <div class="col-sm-offset-2 col-sm-3 col-xs-5">
                                <ul class="list-unstyled list-btc">
                                    <li>0 BTC</li>
                                    <li>500 BTC</li>
                                    <li>750 BTC</li>
                                    <li>1000 BTC</li>
                                    <li>1250 BTC</li>
                                    <li>1500 BTC</li>
                                </ul>
                            </div>
                            <div class="col-sm-2 col-xs-2">
                                <img src="assets/images/timeline-bar-vertical.png" class="img-responsive horizontal-center img-timeline">

                                <div class="img-timeline-wrapper bg-center bg-contain lazy" data-original="assets/images/timeline-bar.png"></div>
                            </div>
                            <div class="col-sm-4 col-xs-5">
                                <ul class="list-unstyled list-btc list-bonus">
                                    <li>
                                        <p>OCT 10-20</p>
                                        <p>+30% Bonus</p>
                                    </li>
                                    <li>
                                        <p>OCT 10-20</p>
                                        <p>+30% Bonus</p>
                                    </li>
                                    <li>
                                        <p>OCT 10-20</p>
                                        <p>+30% Bonus</p>
                                    </li>
                                    <li>
                                        <p>OCT 10-20</p>
                                        <p>+30% Bonus</p>
                                    </li>
                                    <li>
                                        <p>OCT 10-20</p>
                                        <p>+30% Bonus</p>
                                    </li>

                                </ul>
                            </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</section>