<section class="page-header text-white token-sale-event">
    <div class="container header-container">
        <div class="row">
            <div class="col-md-5 col-xs-12">
                <div class="upper-content-block">
                    <h1>Token Sale Event</h1>
                    <p>Oct 12 - Nov 15, 2017</p>
                </div>

                <div class="lower-content-block">
                    <p>Trive is a digital token, where each coin is backed by one gram of gold at launch. In addition, each transaction of OneGram Coin (OGC) generates a small transaction fee which is reinvested in more gold.</p>

                    <ul class="list-unstyled list-inline pdf-links">
                        <li><a href="">Whitepaper</a></li>
                        <li><a href="">Slide Deck</a></li>
                        <li><a href="">Tokenomics</a></li>

                    </ul>
                </div>

            </div>
            <div class="col-md-offset-1 col-md-6 col-xs-12">
                <div class="pre-sale-starting text-center">
                    <h1>Pre-sale starting in:</h1>
                    <div class="count-down-clock ico-clock"></div>
                    <p>ŦRV TOKEN SALE 1ST OF OCTOBER</p>
                    <a href="" class="btn btn-white btn-circle">Contribute/Invest</a>
                </div>
            </div>
        </div>
    </div>

</section>


<section class="ico-timeline bg-dark-orange text-white">
    <span>ICO TIMELINE</span>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center text-white">Current Rate: 1785 ŦRV / ETH</h1>
                <div class="horizontal-timeline hidden-xs hidden-sm">
                    <ul class="list-unstyled list-inline list-btc list-round">
                        <li>PRESALE</li>
                        <li>ROUND 1</li>
                        <li>ROUND 2</li>
                        <li>ROUND 3</li>
                        <li>ROUND 4</li>
                        <li>ROUND 5</li>
                    </ul>

                    <img src="assets/images/timeline-bar.png" class="img-responsive horizontal-center img-timeline">

                    <ul class="list-unstyled list-inline list-btc list-bonus">
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                            <p>$0.15c</p>
                        </li>
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                            <p>$0.15c</p>
                        </li>
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                            <p>$0.15c</p>
                        </li>
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                            <p>$0.15c</p>
                        </li>
                        <li>
                            <p>OCT 10-20</p>
                            <p>+30% Bonus</p>
                            <p>$0.15c</p>
                        </li>

                    </ul>
                </div>

            </div>
            <div class="timeline-vertical hidden-md- hidden-lg">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-3 col-xs-5">
                            <ul class="list-unstyled list-btc">
                                <li>PRESALE</li>
                                <li>ROUND 1</li>
                                <li>ROUND 2</li>
                                <li>ROUND 3</li>
                                <li>ROUND 4</li>
                                <li>ROUND 5</li>
                            </ul>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <img src="assets/images/timeline-bar-vertical.png" class="img-responsive horizontal-center img-timeline">

                            <div class="img-timeline-wrapper bg-center bg-contain lazy" data-original="assets/images/timeline-bar.png"></div>
                        </div>
                        <div class="col-sm-4 col-xs-5">
                            <ul class="list-unstyled list-btc list-bonus">
                                <li>
                                    <p>OCT 10-20</p>
                                    <p>+30% Bonus</p>
                                    <p>$0.15c</p>
                                </li>
                                <li>
                                    <p>OCT 10-20</p>
                                    <p>+30% Bonus</p>
                                    <p>$0.15c</p>
                                </li>
                                <li>
                                    <p>OCT 10-20</p>
                                    <p>+30% Bonus</p>
                                    <p>$0.15c</p>
                                </li>
                                <li>
                                    <p>OCT 10-20</p>
                                    <p>+30% Bonus</p>
                                    <p>$0.15c</p>
                                </li>
                                <li>
                                    <p>OCT 10-20</p>
                                    <p>+30% Bonus</p>
                                    <p>$0.15c</p>
                                </li>

                            </ul>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="coin-offering-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="trive-coin">
                    <img src="assets/images/trive-coin.png" class="img-responsive horizontal-center">
                </div>
            </div>

            <div class="col-md-6">
                <div class="content-block">
                    <h1>Coin Offering Details</h1>
                    <?php $coin_details = array(1,2,3,4,5,6); ?>
                    <?php foreach($coin_details as $coin_detail): ?>
                        <div class="row">
                            <div class="col-md-4">
                                <p><strong>ICO End Date:</strong></p>
                            </div>
                            <div class="col-md-8">
                                <p>October 1, Midnight (ET)</p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ico-participate text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Participate</h1>
            </div>
            <div class="col-md-12">
                <div class="content-block text-center">
                    <p><strong>This is the contract address you should send ETH to:</strong></p>
                    <h2>0x5494482969cFbBB04aFfbf204ca8678e7FC82579</h2>
                    <p>1 ETH = 1785 ŦRV (excl bonus). By Participating you agree to the below crowdsale guidelines.</p>
                    <div class="btn-group">
                        <a href="#" class="btn btn-orange btn-guidelines">Guidelines</a>
                        <a href="#" class="btn btn-orange">Scan QR</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="platform-activity-status text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Platform Activity Stats</h1>
                <ul class="list-unstyled list-inline">
                    <li>
                        <p>Posts Reviewed</p>
                        <h1>X00,000</h1>
                    </li>

                    <li>
                        <p>Posts Reviewed</p>
                        <h1>X00,000</h1>
                    </li>

                    <li>
                        <p>Posts Reviewed</p>
                        <h1>X00,000</h1>
                    </li>

                    <li>
                        <p>Posts Reviewed</p>
                        <h1>X00,000</h1>
                    </li>

                    <li>
                        <p>Posts Reviewed</p>
                        <h1>X00,000</h1>
                    </li>

                    <li>
                        <p>Posts Reviewed</p>
                        <h1>X00,000</h1>
                    </li>

                    <li>
                        <p>Posts Reviewed</p>
                        <h1>X00,000</h1>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="timeline-roadmap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Timeline/Roadmap</h1>
            </div>
            <div class="col-md-12">
                <?php $timeline_milestones = array(1,2,3,4,5,6,7); ?>

                <ul class="list-unstyled list-inline horizontal-center text-center">
                    <?php foreach($timeline_milestones as $timeline_milestone): ?>

                    <li>
                        <div class="timeline-milestone">
                            <img src="assets/images/timeline-logo.png">
                            <p>Test</p>
                            <p>Start of Fake news worldwide</p>
                        </div>
                    </li>
                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </div>
</section>

<section class="cogs-behind text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Cogs behind <strong>Trive&trade;</strong></h1>
                <h2>Executive & Advisory Team</h2>
            </div>
        </div>

        <?php
        $team_members = array('1','2','3','4','5','6','7','8','9','10','11');
        $team_member_first_row = '';
        $team_member_second_row = '';
        $index = 0;
        foreach($team_members as $team_member):
            if($index<5):
                $team_member_first_row .= '<div class="col-sm-2">
                                                <div class="team-member-container">
                                                    <a href="#"><svg class="icon icon-linkedin-with-circle"><use xlink:href="#icon-linkedin-with-circle"></use></svg></a>

                                                    <div class="team-member horizontal-center bg-center bg-cover lazy" data-original="assets/images/team/'.$team_member.'.jpg" >

                                                        <div class="team-member-meta">

                                                            <h4>David Mondrus</h4>
                                                            <p>CEO</p>
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>';
            else:
                $team_member_second_row .= '<div class="col-sm-2">
                                                <div class="team-member-container">
                                                   <a href="#"><svg class="icon icon-linkedin-with-circle"><use xlink:href="#icon-linkedin-with-circle"></use></svg></a>

                                                    <div class="team-member horizontal-center bg-center bg-cover lazy" data-original="assets/images/team/'.$team_member.'.jpg">
                                                        <div class="team-member-meta">
                                                            <h4>David Mondrus</h4>
                                                            <p>CEO</p>
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>';
            endif;
            $index++;
        endforeach; ?>

        <div class="row">
            <div class="team executive-team">
                <div class="col-md-offset-1 col-md-10">
                    <div class="row ten-columns">
                        <?php echo $team_member_first_row; ?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <?php echo $team_member_second_row; ?>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Operations Team</h2>
            </div>
            <div class="col-md-12">
                <div class="team operations-team">
                    <div class="row nine-columns">
                        <?php echo $team_member_second_row; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="inital-coin-offering bg-orange text-white lazy bg-center" data-original="assets/images/ico-bg.png">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-block text-center">
                    <h1>Initial Coin Offering <span>(ICO)</span></h1>

                    <p>ICO is an alternative method of investing raising in the form of "crypto-shares".</p>

                    <p>Each crypto-share is presented in a block-chain token from based on "smart" contracts.</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="countdown-to-crowdsale">
                <div class="col-md-offset-1 col-md-3">
                    <h1>Countdown to Crowdsale</h1>
                </div>
                <div class="col-md-4">
                    <div class="count-down-clock text-white"></div>
                </div>
                <div class="col-md-4">
                    <div class="text-center">
                        <a href="#" class="btn btn-white btn-block btn-circle">Join Presale Now</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="faqs bg-dark-orange text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1>Frequently asked questions</h1>
                <p>Here are general questions and answers about cryptocurrency</p>
            </div>


            <div class="col-md-12">
                <div class="panel-group faq-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <svg class="icon icon-plus"><use xlink:href="#icon-plus"></use></svg> Collapsible Group Item #1
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <svg class="icon icon-plus"><use xlink:href="#icon-plus"></use></svg>
                                    Collapsible Group Item #2
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <svg class="icon icon-plus"><use xlink:href="#icon-plus"></use></svg>
                                    Collapsible Group Item #3
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                            </div>
                        </div>
                    </div>

                </div><!-- panel-group -->
            </div>



            <div class="col-md-offset-2 col-md-8">
                <div class="get-news text-center">
                    <h1>Get News About The Token Sale</h1>
                    <input type="email" class="form-control" value="Email Address">
                </div>
            </div>
        </div>
    </div>

</section>