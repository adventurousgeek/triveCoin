var myLazyLoad = new LazyLoad({
    elements_selector: ".lazy"
});

var myLazyLoad_Images = new LazyLoad();

$(".count-down-clock").countdown("2017/12/15", function(event) {
    $(this).html(
        event.strftime(''
            + '<div class="clock-count"><h3>%D</h3> <h4>DAYS</h4></div>'
            + '<div class="clock-count"><h3>%H</h3> <h4>HRS</h4></div>'
            + '<div class="clock-count"><h3>%M</h3> <h4>MINS</h4></div>'
            + '<div class="clock-count"><h3>%S</h3> <h4>SECS</h4></div>'
        )
    );
});

