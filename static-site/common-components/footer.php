

	<footer class="site-footer">
        <div class="pre-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-7">
                        <div class="footer-widget">
                            <h1>Have questions?</h1>
                            <p>Please reach out to us and we will get<br>
                                back to you as soon as possible.</p>
                        </div>

                    </div>
                    <div class="col-md-6 col-sm-5">
                        <div class="footer-widget text-right">
                            <h1>Resources</h1>
                            <ul class="menu">
                                <li><a href="">Whitepaper</a></li>
                                <li><a href="">Slide Deck</a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="post-footer">
            <div class="container">
                <div class="row">

                    <div class="col-md-3 col-sm-4">
                        <ul class="list-unstyled list-inline footer-social-menu">
                            <li><a href="#"><svg class="icon icon-facebook"><use xlink:href="#icon-facebook"></use></svg></a></li>
                            <li><a href="#"><svg class="icon icon-twitter"><use xlink:href="#icon-twitter"></use></svg></a></li>
                            <li><a href="#"><svg class="icon icon-linkedin2"><use xlink:href="#icon-linkedin2"></use></svg></a></li>
                            <li><a href="#"><svg class="icon icon-paperplane"><use xlink:href="#icon-paperplane"></use></svg></a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 col-sm-4">
                        <div class="footer-logo">
                            <img src="assets/images/footer-logo.png" class="img-responsive">
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-4">
                        <p class="copyright">© 2017 Copyrights</p>
                    </div>
                </div>
            </div>
        </div>

	</footer>
    <script async src="assets/js/scripts.js"></script>

    </body>
</html>



