<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <title>Trive</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <style>
          <?php
              $css = file_get_contents('style.css');
              //echo $css;
          ?>
    </style>
  </head>
<body>
<?php require_once "svg-icons.php"; ?>

	<header class="site-header">
		<nav class="navbar navbar-default navbar-fixed-top">
		  <div class="container header-container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="#">
                  <img src="assets/images/logo.png" class="img-responsive">
              </a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">

                <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>

                <ul class="nav navbar-nav navbar-right hidden">
                  <li><a href="#">HOME</a></li>
                  <li><a href="#">ABOUT</a></li>
                  <li><a href="#">VENUE</a></li>
			  </ul>
			</div><!--/.nav-collapse -->
		  </div>
		</nav>
	</header>

