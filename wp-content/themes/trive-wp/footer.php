

<footer class="site-footer">
    <div class="pre-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-7">
                    <div class="footer-widget">
                        <h1><?php echo options()->get_field( 'footer_about_heading' ) ?></h1>
                        <p><?php echo options()->get_field( 'footer_about_content' ) ?></p>
                    </div>

                </div>
                <div class="col-md-6 col-sm-5">
                    <div class="footer-widget text-right">
                        <h1><?php echo options()->get_field( 'footer_resources_heading' ) ?></h1>
                        <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-right' ) ); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="post-footer">
        <div class="container">
            <div class="row">

                <div class="col-md-3 col-sm-4">
                    <ul class="list-unstyled list-inline footer-social-menu">
                        <li><a href="<?php echo options()->get_field( 'facebook_link' ) ?>" target="_blank"><svg class="icon icon-facebook"><use xlink:href="#icon-facebook"></use></svg></a></li>
                        <li><a href="<?php echo options()->get_field( 'twitter_link' ) ?>" target="_blank"><svg class="icon icon-twitter"><use xlink:href="#icon-twitter"></use></svg></a></li>
                        <li><a href="<?php echo options()->get_field( 'linkedin_link' ) ?>" target="_blank"><svg class="icon icon-linkedin2"><use xlink:href="#icon-linkedin2"></use></svg></a></li>
                        <li><a href="mailto:<?php echo options()->get_field( 'email_link' ) ?>" target="_blank"><svg class="icon icon-paperplane"><use xlink:href="#icon-paperplane"></use></svg></a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-4">
                    <div class="footer-logo">
                        <img data-original="<?php echo get_template_directory_uri();?>/assets/images/footer-logo.png" class="img-responsive">
                    </div>
                </div>

                <div class="col-md-3 col-sm-4">
                    <p class="copyright"><?php echo options()->get_field( 'footer_copyright' ) ?></p>
                </div>
            </div>
        </div>
    </div>

</footer>
<script async src="<?php echo get_template_directory_uri();?>/assets/js/scripts.js"></script>
<?php wp_footer(); ?>
</body>
</html>



