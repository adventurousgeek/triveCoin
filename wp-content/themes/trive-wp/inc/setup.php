<?php

class trive_Setup
{

    public static function bootstrap()
    {
        self::requirements();
        self::includes();
        self::setup_options();
        self::setup_fields();
        self::remove_extra_code();
        self::theme_support();
        self::theme_navigations();
    }

    protected static function requirements()
    {
        if (self::is_admin()) {
            return;
        }

        if (!function_exists('register_field_group')) {
            die("Please activate required plugins before using this theme.");
        }
    }

    protected static function is_admin()
    {
        return (is_admin() OR ($GLOBALS['pagenow'] === "wp-login.php"));
    }

    protected static function includes()
    {
        $acf_helpers = array('ACFFieldItem', 'ACFLocation', 'ACFFieldHelper', 'ACFHide', 'ACFMetabox');
        foreach ($acf_helpers as $acf_helper_class) {
            if (!class_exists($acf_helper_class)) {
                require_once get_stylesheet_directory() . "/inc/acf/$acf_helper_class.php";
            }
        }
    }

    protected static function setup_options()
    {
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(array(
                'page_title' => 'Options',
                'menu_title' => 'Options',
                'menu_slug' => 'options',
                'capability' => 'edit_posts',
                'redirect' => FALSE
            ));
        }
    }

    protected static function setup_fields()
    {
        $acf_fields = glob(get_stylesheet_directory() . "/acf/*.php");
        foreach ($acf_fields as $acf_field) {
            self::include_file($acf_field);
        }
    }

    protected static function include_file($file)
    {
        require_once $file;
    }

    protected static function remove_extra_code(){

        remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
        remove_action('wp_head', 'wp_generator'); // remove wordpress version

        remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
        remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links

        remove_action('wp_head', 'index_rel_link'); // remove link to index page
        remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)

        remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
        remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
        remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
        remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );

        // REMOVE WP EMOJI
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_print_styles', 'print_emoji_styles');

        remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        remove_action( 'admin_print_styles', 'print_emoji_styles' );


        remove_action( 'wp_head', 'wp_resource_hints', 2 );

        // Disable REST API link tag
        remove_action('wp_head', 'rest_output_link_wp_head', 10);

        // Disable oEmbed Discovery Links
        remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

        // Disable REST API link in HTTP headers
        remove_action('template_redirect', 'rest_output_link_header', 11, 0);


        // Remove the REST API endpoint.
        remove_action( 'rest_api_init', 'wp_oembed_register_route' );

        // Turn off oEmbed auto discovery.
        add_filter( 'embed_oembed_discover', '__return_false' );

        // Don't filter oEmbed results.
        remove_filter( 'oembed_dataparse', 'wp_filter_oembed_result', 10 );

        // Remove oEmbed discovery links.
        remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );

        // Remove oEmbed-specific JavaScript from the front-end and back-end.
        remove_action( 'wp_head', 'wp_oembed_add_host_js' );

    }

    public static function theme_support(){
        add_theme_support( 'title-tag' );

    }

    public static function theme_navigations(){

        register_nav_menus(
            array('footer-menu-right' => __('Footer Menu', 'trive'))
        );

        register_nav_menus(
            array('main-menu' => __('Main Menu', 'trive'))
        );

    }
}


trive_Setup::bootstrap();
