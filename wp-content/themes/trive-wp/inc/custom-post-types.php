<?php

// Team Post Type
function team_post_type() {

    $labels = array(
        'name'                  => _x( 'Team', 'Post Type General Name', 'trive' ),
        'singular_name'         => _x( 'Team', 'Post Type Singular Name', 'trive' ),
        'menu_name'             => __( 'Team', 'trive' ),
        'name_admin_bar'        => __( 'Team', 'trive' ),
        'archives'              => __( 'Item Archives', 'trive' ),
        'attributes'            => __( 'Item Attributes', 'trive' ),
        'parent_item_colon'     => __( 'Parent Team:', 'trive' ),
        'all_items'             => __( 'All Items', 'trive' ),
        'add_new_item'          => __( 'Add New Item', 'trive' ),
        'add_new'               => __( 'New Item', 'trive' ),
        'new_item'              => __( 'New Item', 'trive' ),
        'edit_item'             => __( 'Edit Team', 'trive' ),
        'update_item'           => __( 'Update Team', 'trive' ),
        'view_item'             => __( 'View Team', 'trive' ),
        'view_items'            => __( 'View Items', 'trive' ),
        'search_items'          => __( 'Search team', 'trive' ),
        'not_found'             => __( 'No team found', 'trive' ),
        'not_found_in_trash'    => __( 'No team found in Trash', 'trive' ),
        'featured_image'        => __( 'Featured Image', 'trive' ),
        'set_featured_image'    => __( 'Set featured image', 'trive' ),
        'remove_featured_image' => __( 'Remove featured image', 'trive' ),
        'use_featured_image'    => __( 'Use as featured image', 'trive' ),
        'insert_into_item'      => __( 'Insert into item', 'trive' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'trive' ),
        'items_list'            => __( 'Items list', 'trive' ),
        'items_list_navigation' => __( 'Items list navigation', 'trive' ),
        'filter_items_list'     => __( 'Filter items list', 'trive' ),
    );

    $args = array(
        'label'                 => __( 'Team', 'trive' ),
        'description'           => __( 'Team information pages.', 'trive' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'team', $args );


}
add_action( 'init', 'team_post_type', 0 );

// News Post Type
function news_post_type() {

    $labels = array(
        'name'                  => _x( 'News', 'Post Type General Name', 'trive' ),
        'singular_name'         => _x( 'News', 'Post Type Singular Name', 'trive' ),
        'menu_name'             => __( 'News', 'trive' ),
        'name_admin_bar'        => __( 'News', 'trive' ),
        'archives'              => __( 'Item Archives', 'trive' ),
        'attributes'            => __( 'Item Attributes', 'trive' ),
        'parent_item_colon'     => __( 'Parent News:', 'trive' ),
        'all_items'             => __( 'All Items', 'trive' ),
        'add_new_item'          => __( 'Add New Item', 'trive' ),
        'add_new'               => __( 'New Item', 'trive' ),
        'new_item'              => __( 'New Item', 'trive' ),
        'edit_item'             => __( 'Edit News', 'trive' ),
        'update_item'           => __( 'Update News', 'trive' ),
        'view_item'             => __( 'View News', 'trive' ),
        'view_items'            => __( 'View Items', 'trive' ),
        'search_items'          => __( 'Search news', 'trive' ),
        'not_found'             => __( 'No news found', 'trive' ),
        'not_found_in_trash'    => __( 'No news found in Trash', 'trive' ),
        'featured_image'        => __( 'Featured Image', 'trive' ),
        'set_featured_image'    => __( 'Set featured image', 'trive' ),
        'remove_featured_image' => __( 'Remove featured image', 'trive' ),
        'use_featured_image'    => __( 'Use as featured image', 'trive' ),
        'insert_into_item'      => __( 'Insert into item', 'trive' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'trive' ),
        'items_list'            => __( 'Items list', 'trive' ),
        'items_list_navigation' => __( 'Items list navigation', 'trive' ),
        'filter_items_list'     => __( 'Filter items list', 'trive' ),
    );

    $args = array(
        'label'                 => __( 'News', 'trive' ),
        'description'           => __( 'News information pages.', 'trive' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'news', $args );


}
add_action( 'init', 'news_post_type', 1 );

