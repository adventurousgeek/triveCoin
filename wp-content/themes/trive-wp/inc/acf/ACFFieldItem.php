<?php

    class ACFFieldItem
    {

        public $key = "";
        public $label = "";
        public $name = "";
        public $prefix = "";
        public $type = "";
        public $instructions = "";
        public $required = "";
        public $conditional_logic = array();
        public $default_value = "";
        public $placeholder = "";
        public $prepend = "";
        public $append = "";
        public $maxlength = "";
        public $readonly = "";
        public $disabled = "";
        public $tabs = "";
        public $toolbar = "";
        public $media_upload = "";
        public $width = "";
        public $height = "";
        public $return_format = "";
        public $preview_size = "";
        public $library = "";
        public $allow_null = "";
        public $multiple = "";
        public $layouts = "";
        public $layout = "";
        public $min = "";
        public $max = "";
        public $button_label = "Add Row";
        public $message = "";
        public $rows = "";
        public $new_lines = "";
        public $choices = "";
        public $post_type = "";

        public function __construct(array $settings = NULL)
        {
            if (!empty($settings)) {
                foreach ($settings as $key => $value) {
                    $this->{$key} = $value;
                }
            }
        }

        public function key($key)
        {
            $this->key = $key;

            return $this;
        }

        public function label($label)
        {
            $this->label = $label;

            return $this;
        }

        public function name($name)
        {
            $this->name = $name;

            return $this;
        }

        public function prefix($prefix)
        {
            $this->prefix = $prefix;

            return $this;
        }

        public function type($type)
        {
            $this->type = $type;

            return $this;
        }

        public function instructions($instructions)
        {
            $this->instructions = $instructions;

            return $this;
        }

        public function required($required)
        {
            $this->required = $required;

            return $this;
        }

        public function conditional_logic($conditional_logic)
        {
            $this->conditional_logic = $conditional_logic;

            return $this;
        }

        public function condition($field, $value, $operator = '==', $key = 0)
        {
            if(!is_array($this->conditional_logic))
            {
                $this->conditional_logic = array();
            }
            $this->conditional_logic[$key][] = compact('field', 'value', 'operator');

            return $this;
        }

        public function default_value($default_value)
        {
            $this->default_value = $default_value;

            return $this;
        }

        public function placeholder($placeholder)
        {
            $this->placeholder = $placeholder;

            return $this;
        }

        public function append($append)
        {
            $this->append = $append;

            return $this;
        }

        public function maxlength($maxlength)
        {
            $this->maxlength = $maxlength;

            return $this;
        }

        public function readonly($readonly)
        {
            $this->readonly = $readonly;

            return $this;
        }

        public function disabled($disabled)
        {
            $this->disabled = $disabled;

            return $this;
        }

        public function tabs($tabs)
        {
            $this->tabs = $tabs;

            return $this;
        }

        public function toolbar($toolbar)
        {
            $this->toolbar = $toolbar;

            return $this;
        }

        public function media_upload($media_upload)
        {
            $this->media_upload = $media_upload;

            return $this;
        }

        public function width($width)
        {
            $this->width = $width;

            return $this;
        }

        public function height($height)
        {
            $this->height = $height;

            return $this;
        }

        public function return_format($return_format)
        {
            $this->return_format = $return_format;

            return $this;
        }

        public function preview_size($preview_size)
        {
            $this->preview_size = $preview_size;

            return $this;
        }

        public function library($library)
        {
            $this->library = $library;

            return $this;
        }

        public function allow_null($allow_null)
        {
            $this->allow_null = $allow_null;

            return $this;
        }
        public function multiple($multiple)
        {
            $this->multiple = $multiple;

            return $this;
        }

        public function layouts($layouts)
        {
            $this->layouts = $layouts;

            return $this;
        }

        public function sub_field($field_name, ACFFieldItem $field)
        {
            $this->sub_fields[$field_name] = $field;

            return $this;
        }

        public function min($min)
        {
            $this->min = $min;

            return $this;
        }

        public function max($max)
        {
            $this->max = $max;

            return $this;
        }

        public function block()
        {
            $this->layout = "row";

            return $this;
        }

        public function table()
        {
            $this->layout = "table";

            return $this;
        }

        public function button_label($button_label)
        {
            $this->button_label = $button_label;

            return $this;
        }

        public function message($message)
        {
            $this->message = $message;

            return $this;
        }

        public function rows($rows)
        {
            $this->rows = $rows;

            return $this;
        }

        public function new_lines($new_lines = TRUE)
        {
            $this->new_lines = $new_lines ? "wpautop" : "";

            return $this;
        }

        public function choices($choices = array())
        {
            $this->choices = $choices;

            return $this;
        }

        public function choice($label, $key = NULL)
        {
            $args = func_get_args();
            if (count($args) > 2) {
                foreach ($args as $choice) {
                    $this->choice($choice);
                }

                return $this;
            }

            $key                 = empty($key) ? $label : $key;
            $this->choices[$key] = $label;

            return $this;
        }

        public function post_type()
        {
            $this->post_type = func_get_args();
            return $this;
        }
    }
