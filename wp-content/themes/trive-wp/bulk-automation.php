<?php
/*
Template Name: Bulk Process
*/
get_header();

exit;

$dir = get_template_directory().'/assets/images/our-gallery/';

if (is_dir($dir)){
    if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){
            if($file=='.' || $file=='..'){
                continue;
            }

            $title = ucwords(str_replace('.jpg','',str_replace('-', ' ',$file)));

            $post_id = wp_insert_post(array (
                'post_type' => 'gallery',
                'post_title' => $title,
                'post_status' => 'publish',
                'comment_status' => 'closed',
                'ping_status' => 'closed',
            ));

            $fields_map = array(
                'gallery_item_caption' 				=>  $title,
            );

            $image_path = $dir.$file;
            $attachment_id = upload_media(0,$image_path, $file);
            update_post_meta( $post_id, 'gallery_item_image', $attachment_id );
            update_post_meta( $post_id, '_gallery_item_image', 'field_partner_listing_gallery_item_image' );



            foreach($fields_map as $destination => $source ){
                update_post_meta( $post_id, $destination, $source );
                update_post_meta( $post_id, '_'.$destination, 'field_gallery_listing_'.$destination );
            }



        }
        closedir($dh);
    }
}



function upload_media($parent_post_id, $file, $filename){

    $upload_file = wp_upload_bits($filename, null, file_get_contents($file));
    if (!$upload_file['error']) {
        $wp_filetype = wp_check_filetype($filename, null );
        $attachment = array(
            'post_mime_type' => $wp_filetype['type'],
            'post_parent' => $parent_post_id,
            'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
            'post_content' => '',
            'post_status' => 'inherit'
        );
        $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $parent_post_id );
        if (!is_wp_error($attachment_id)) {
            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
            wp_update_attachment_metadata( $attachment_id,  $attachment_data );
        }
        return $attachment_id;
    }

}


get_footer();
?>