<?php get_header(); ?>

<section class="text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php _e( 'Not Found', 'trive' ); ?></h1>
            </div>
            <div class="col-md-12">
                <p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'trive' ); ?></p>
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>