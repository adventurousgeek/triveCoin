<?php
/*
Template Name: ICO
*/
get_header();
?>

<section class="page-header text-white token-sale-event">

    <svg class="header-map"><use xlink:href="#icon-map"></use></svg>
    <div class="container header-container">
        <div class="row">
            <div class="col-md-5 col-xs-12">
                <div class="upper-content-block">
                    <h1><?php the_field('ico_banner_heading'); ?></h1>
                    <p><?php the_field('ico_banner_date'); ?></p>
                </div>

                <div class="lower-content-block">
                    <?php the_field('ico_banner_content'); ?>
                    <?php $banner_list = get_field('ico_banner_list'); ?>
                    <ul class="list-unstyled list-inline pdf-links">
                        <?php foreach ($banner_list as $item): ?>
                            <li><a href="<?php echo $item['url']; ?>"><?php echo $item['item']; ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>

            </div>
            <div class="col-md-offset-1 col-md-6 col-xs-12">
                <div class="pre-sale-starting text-center">
                    <h1><?php the_field('ico_banner_countdown_heading'); ?></h1>
                    <div class="count-down-clock ico-clock"></div>
                    <p><?php the_field('ico_banner_countdown_description'); ?></p>
                    <a href="<?php the_field('ico_banner_button_url'); ?>" class="btn btn-white btn-circle"><?php the_field('ico_banner_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>

</section>


<section class="ico-timeline bg-dark-orange text-white">
    <span><?php the_field('ico_timeline_ico_heading'); ?></span>
    <?php $btc_list     = get_field('ico_timeline_ico_btc_list'); ?>
    <?php $bonus_list   = get_field('ico_timeline_ico_bonus_list'); ?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center text-white"><?php the_field('ico_timeline_ico_sub_heading'); ?></h1>
                <div class="horizontal-timeline hidden-xs hidden-sm">
                    <ul class="list-unstyled list-inline list-btc list-round">
                        <?php foreach($btc_list as $item): ?>
                            <li><?php echo $item['rounds']; ?></li>
                        <?php endforeach; ?>
                    </ul>

                    <img  data-original="<?php echo get_template_directory_uri();?>/assets/images/timeline-bar.png" class="img-responsive horizontal-center img-timeline">

                    <ul class="list-unstyled list-inline list-btc list-bonus">
                        <?php foreach($bonus_list as $item): ?>
                            <li>
                                <p><?php echo $item['date']; ?></p>
                                <p><?php echo $item['bonus']; ?></p>
                                <p><?php echo $item['amount']; ?></p>
                            </li>
                        <?php endforeach; ?>


                    </ul>
                </div>

            </div>
            <div class="timeline-vertical hidden-md- hidden-lg">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-3 col-xs-5">
                            <ul class="list-unstyled list-btc">
                                <?php foreach($btc_list as $item): ?>
                                    <li><?php echo $item['rounds']; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <img  data-original="<?php echo get_template_directory_uri();?>/assets/images/timeline-bar-vertical.png" class="img-responsive horizontal-center img-timeline">

                            <div class="img-timeline-wrapper bg-center bg-contain lazy" data-original="<?php echo get_template_directory_uri();?>/assets/images/timeline-bar.png"></div>
                        </div>
                        <div class="col-sm-4 col-xs-5">
                            <ul class="list-unstyled list-btc list-bonus">
                                <?php foreach($bonus_list as $item): ?>
                                    <li>
                                        <p><?php echo $item['date']; ?></p>
                                        <p><?php echo $item['bonus']; ?></p>
                                        <p><?php echo $item['amount']; ?></p>
                                    </li>
                                <?php endforeach; ?>

                            </ul>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="coin-offering-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="trive-coin">

                    <div class="coin">
                        <div class="side heads lazy"  data-original="<?php echo get_template_directory_uri();?>/assets/images/trive-coin.png"></div>
                        <div class="side tails lazy"  data-original="<?php echo get_template_directory_uri();?>/assets/images/trive-coin.png"></div>
                    </div>

                    <img  data-original="<?php echo get_template_directory_uri();?>/assets/images/trive-coin.png" class="img-responsive horizontal-center hidden">
                </div>
            </div>

            <div class="col-md-6 col-sm-6">
                <div class="content-block">
                    <h1><?php echo the_field('coin_offering_heading'); ?></h1>

                    <?php $coin_details = get_field('coin_offering_list'); ?>
                    <?php foreach($coin_details as $coin_detail): ?>
                        <div class="row">
                            <div class="col-md-4">
                                <p><strong><?php echo $coin_detail['heading']; ?></strong></p>
                            </div>
                            <div class="col-md-8">
                                <p><?php echo $coin_detail['value']; ?></p>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ico-participate text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php the_field('participate_heading'); ?></h1>
            </div>
            <div class="col-md-12">
                <div class="content-block text-center">
                    <p><strong><?php the_field('participate_sub_heading'); ?></strong></p>
                    <h2><?php the_field('participate_eth'); ?></h2>
                    <p><?php the_field('participate_paragraph'); ?></p>
                    <div class="btn-group">
                        <a href="<?php the_field('participate_guideline_link'); ?>" class="btn btn-orange btn-guidelines" data-clipboard-text="<?php the_field('participate_eth'); ?>"><?php the_field('participate_guideline_text'); ?></a>
                        <a href="<?php the_field('participate_scanqr_link'); ?>" class="btn btn-orange btn-scan-qr"><?php the_field('participate_scanqr_text'); ?></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="platform-activity-status text-center">
    <div id="particles-js"></div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php the_field('platform_activity_heading'); ?></h1>
                <?php $activity_list = get_field('platform_activity_list'); ?>
                <ul class="list-unstyled list-inline">
                    <?php foreach($activity_list as $item): ?>
                    <li>
                        <p><?php echo $item['heading']; ?></p>
                        <h1><?php echo $item['value']; ?></h1>
                    </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<section class="timeline-roadmap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center"><?php the_field('roadmap_heading'); ?></h1>
            </div>
            <div class="col-md-12">
                <?php $timeline_milestones = get_field('roadmap_list'); ?>

                <ul class="list-unstyled list-inline horizontal-center text-center">
                    <?php foreach($timeline_milestones as $timeline_milestone): ?>


                        <li>
                            <div class="timeline-milestone">
                                <img  data-original="<?php echo $timeline_milestone['image']['url']; ?>">
                                <p><?php echo $timeline_milestone['date']; ?></p>
                                <p><?php echo $timeline_milestone['description']; ?></p>
                            </div>
                        </li>
                    <?php endforeach; ?>

                </ul>
            </div>
        </div>
    </div>
</section>

<?php get_template_part( 'page-templates/cogs-behind'); ?>

<section class="inital-coin-offering bg-orange text-white lazy bg-center" data-original="<?php echo get_template_directory_uri();?>/assets/images/ico-bg.png">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-block text-center">
                    <h1><?php the_field('ico_initial_coin_offering_heading'); ?></h1>
                    <?php the_field('ico_initial_coin_offering_content'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="countdown-to-crowdsale">
                <div class="col-md-offset-1 col-md-3">
                    <h1><?php the_field('ico_inital_coin_offering_countdown_heading')?></h1>
                </div>
                <div class="col-md-4">
                    <div class="count-down-clock text-white"></div>
                </div>
                <div class="col-md-4">
                    <div class="text-center">
                        <a href="<?php the_field('ico_inital_coin_offering_button_url')?>" class="btn btn-white btn-block btn-circle"><?php the_field('ico_inital_coin_offering_button_text')?></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="faqs bg-dark-orange text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1><?php the_field('faqs_heading');?></h1>
                <p><?php the_field('faqs_sub_heading');?></p>
            </div>


            <div class="col-md-12">
                <div class="panel-group faq-group" id="accordion" role="tablist" aria-multiselectable="true">

                    <?php $faqs = get_field('faqs_list'); ?>
                    <?php $faq_index = 1;?>
                    <?php foreach($faqs as $faq): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading-faq-<?php echo $faq_index; ?>">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#faq-<?php echo $faq_index; ?>" aria-expanded="true" aria-controls="faq-<?php echo $faq_index; ?>">
                                    <svg class="icon icon-plus"><use xlink:href="#icon-plus"></use></svg> <?php echo $faq['question']; ?>
                                </a>
                            </h4>
                        </div>
                        <div id="faq-<?php echo $faq_index; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading-faq-<?php echo $faq_index; ?>">
                            <div class="panel-body">
                                <?php echo $faq['answer']; ?>
                            </div>
                        </div>
                    </div>
                    <?php $faq_index++; ?>
                    <?php endforeach; ?>



                </div><!-- panel-group -->
            </div>



            <div class="col-md-offset-2 col-md-8">
                <div class="get-news text-center">
                    <h1><?php the_field('get_news_heading'); ?></h1>
                    <?php echo do_shortcode('[contact-form-7 id="44" title="Email Form"]'); ?>
                </div>
            </div>
        </div>
    </div>

</section>


<div id="copy-confirmation" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Information</h4>
            </div>
            <div class="modal-body">
                <p>Address has been copied to clipboard</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-orange btn-circle btn-modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="scan-qr" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">QR Code</h4>
            </div>
            <div class="modal-body">
                <img src="<?php echo get_template_directory_uri();?>/assets/images/qr-code.png" class="img-responsive horizontal-center">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-orange btn-circle btn-modal" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>

