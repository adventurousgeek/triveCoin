
<section class="cogs-behind text-center">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1><?php echo options()->get_field( 'cogs_behind_heading' ) ?></h1>
                <h2><?php echo options()->get_field('cogs_behind_executive_heading'); ?></h2>
            </div>
        </div>

        <?php
        $team_member= '';
        $index = 1;

        $args = array(
            'post_type'         => 'team',
            'posts_per_page'    => -1,
            'order'             => 'DESC',
            'meta_key'		    => 'team_member_team',
            'meta_value'	    => 'advisory'
        );

        $the_query = new WP_Query( $args );
        while ( $the_query->have_posts() ) :
            $the_query->the_post();
            $team_member_image = get_field('team_member_image');
            $team_class = '';
            //echo (12+($index-1)*11).'<br>';
            if(12+($index-1)*11 == $index):
                $team_class = 'wrap-column';
            endif;

            $team_member .= '<div class="col-md-2 col-sm-3">
                                                <div class="team-member-container">
                                                    <a target="_blank" href="'.get_field('team_member_linkedin').'"><svg class="icon icon-linkedin-with-circle"><use xlink:href="#icon-linkedin-with-circle"></use></svg></a>

                                                    <div class="team-member horizontal-center bg-center bg-cover lazy" data-original="'.$team_member_image['url'].'" >

                                                        <div class="team-member-meta">

                                                            <h4>'.get_the_title().'</h4>
                                                            <p>'.get_field('team_member_designation').'</p>
                                                        </div>
                                                    </div>
                                                </div>
                                          </div>';

            $index++;
        endwhile;
        wp_reset_postdata();
        ?>

        <div class="row">
            <div class="team executive-team">


                <div class="col-md-12">
                    <div class="row team-row">
                        <?php echo $team_member; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2><?php echo options()->get_field('cogs_behind_operations_heading'); ?></h2>
            </div>
            <div class="col-md-12">
                <div class="team operations-team">
                    <div class="row nine-columns">

                        <?php

                        $args = array(
                            'post_type'         => 'team',
                            'posts_per_page'    => -1,
                            'order'             => 'DESC',
                            'meta_key'		    => 'team_member_team',
                            'meta_value'	    => 'operations'
                        );

                        $the_query = new WP_Query( $args );
                        while ( $the_query->have_posts() ) :
                            $the_query->the_post();
                            $team_member_image = get_field('team_member_image');
                            ?>
                            <div class="col-sm-2">
                                <div class="team-member-container">
                                    <a target="_blank" href="<?php the_field('team_member_linkedin'); ?>"><svg class="icon icon-linkedin-with-circle"><use xlink:href="#icon-linkedin-with-circle"></use></svg></a>

                                    <div class="team-member horizontal-center bg-center bg-cover lazy" data-original="<?php  echo $team_member_image['url']; ?>">
                                        <div class="team-member-meta">
                                            <h4><?php the_title(); ?></h4>
                                            <p><?php the_field('team_member_designation'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        endwhile;
                        wp_reset_postdata();

                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>