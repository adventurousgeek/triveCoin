<?php
/*
Template Name: Home
*/
get_header();
?>
<section class="page-header text-white truth-discovered">
    <canvas></canvas>
    <canvas></canvas>
    <div class="container header-container">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <h1><?php the_field('banner_heading'); ?></h1>
                <p><?php the_field('banner_sub_heading'); ?></p>
            </div>
            <div class="col-md-6 col-xs-12">
                <a href="<?php the_field('banner_button_url'); ?>" class="btn btn-download btn-orange btn-circle"><svg class="icon-chrome"><use xlink:href="#icon-chrome"></use></svg><?php the_field('banner_button_text'); ?></a>
            </div>
        </div>
    </div>
    <div class="container header-container">
        <ul class="latest-news list-unstyled list-inline">
            <?php

            $args = array(
                'post_type'         => 'news',
                'posts_per_page'    => -1,
                'order'             => 'DESC',
            );

            $the_query = new WP_Query( $args );
            while ( $the_query->have_posts() ) :
            $the_query->the_post();
            $news_image = get_field('news_image');
            ?>
                <li class="latest-news-item text-center">
                    <a href="<?php the_field('news_link') ?>">
                        <div class="news-thumbnail bg-center bg-cover lazy" data-original="<?php echo $news_image['url']; ?>"></div>
                        <div class="news-meta">
                            <h2><?php the_title(); ?></h2>
                            <p><?php the_field('news_description'); ?></p>
                        </div>
                    </a>
                </li>
            <?php
            endwhile;
            wp_reset_postdata();
            ?>


        </ul>
    </div>
</section>

<section class="countdown-to-crowdsale">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-3">
                <h1><?php the_field('main_countdown_heading'); ?></h1>
            </div>
            <div class="col-md-4">
                <div class="count-down-clock"></div>
            </div>
            <div class="col-md-3">
                <div class="text-center">
                    <a href="<?php the_field('main_countdown_button_url'); ?>" class="btn btn-orange btn-circle"><?php the_field('main_countdown_button_text'); ?></a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="how-we-help">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="intro-text text-dark-gray text-center">
                    <h1><?php the_field('main_info_heading'); ?></h1>
                    <?php the_field('main_info_content'); ?>
                </div>
            </div>
        </div>
    </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="trive-features bg-contain bg-center lazy" data-original="<?php echo get_template_directory_uri();?>/assets/images/trive-features.jpg">
                    <div class="row">

                        <div class="col-sm-12 hidden-md hidden-lg">
                            <img  data-original="<?php echo get_template_directory_uri();?>/assets/images/trive-plugin.png" class="img-resposnsive">
                        </div>
                        <div class="col-md-offset-6 col-md-6 col-sm-12">
                            <h1><?php the_field('trive_plugin_heading'); ?></h1>
                            <?php the_field('trive_plugin_content'); ?>
                            <a href="<?php the_field('trive_plugin_link_url'); ?>"><?php the_field('trive_plugin_link_text'); ?> <svg class="icon icon-chevron-with-circle-right"><use xlink:href="#icon-chevron-with-circle-right"></use></svg></a>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12 hidden-md hidden-lg">
                            <img  data-original="<?php echo get_template_directory_uri();?>/assets/images/trive-engine.png" class="img-resposnsive">
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <h1><?php the_field('trive_engine_heading'); ?></h1>
                            <?php the_field('trive_engine_content'); ?>
                            <a href="<?php the_field('trive_engine_link_url'); ?>"><?php the_field('trive_engine_link_text'); ?> <svg class="icon icon-chevron-with-circle-right"><use xlink:href="#icon-chevron-with-circle-right"></use></svg></a>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-sm-12 hidden-md hidden-lg">
                            <img  data-original="<?php echo get_template_directory_uri();?>/assets/images/trive-participants.png" class="img-resposnsive">
                        </div>
                        <div class="col-md-offset-6 col-md-6 col-sm-12">
                            <h1><?php the_field('trive_participants_heading'); ?></h1>
                            <?php the_field('trive_participants_content'); ?>
                            <a href="<?php the_field('trive_participants_link_url'); ?>"><?php the_field('trive_participants_link_text'); ?> <svg class="icon icon-chevron-with-circle-right"><use xlink:href="#icon-chevron-with-circle-right"></use></svg></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_template_part( 'page-templates/cogs-behind'); ?>

<section class="inital-coin-offering bg-orange text-white lazy bg-center" data-original="<?php echo get_template_directory_uri();?>/assets/images/ico-bg.png">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="content-block text-center">
                    <h1><?php the_field('initial_coin_offering_heading'); ?></h1>
                    <?php the_field('initial_coin_offering_content'); ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="countdown-to-crowdsale">
                <div class="col-md-offset-1 col-md-3">
                    <h1><?php the_field('inital_coin_offering_countdown_heading')?></h1>
                </div>
                <div class="col-md-4">
                    <div class="count-down-clock text-white"></div>
                </div>
                <div class="col-md-4">
                    <div class="text-center">
                        <a href="<?php the_field('inital_coin_offering_button_url')?>" class="btn btn-white btn-block btn-circle"><?php the_field('inital_coin_offering_button_text')?></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="ico-timeline bg-dark-orange text-white">
    <span><?php the_field('homepage_timeline_ico_heading'); ?></span>
    <?php $btc_list     = get_field('homepage_timeline_ico_btc_list'); ?>
    <?php $bonus_list   = get_field('homepage_timeline_ico_bonus_list'); ?>
    <div class="container">
        <div class="row">
            <div class="horizontal-timeline hidden-xs hidden-sm">
                <div class="col-md-12">
                    <ul class="list-unstyled list-inline list-btc">
                        <?php foreach($btc_list as $item): ?>
                            <li><?php echo $item['btc']; ?></li>
                        <?php endforeach; ?>
                    </ul>

                    <img  data-original="<?php echo get_template_directory_uri();?>/assets/images/timeline-bar.png" class="img-responsive horizontal-center img-timeline">

                    <ul class="list-unstyled list-inline list-btc list-bonus">
                        <?php foreach($bonus_list as $item): ?>
                            <li>
                                <p><?php echo $item['date']; ?></p>
                                <p><?php echo $item['bonus']; ?></p>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>

            </div>

            <div class="timeline-vertical hidden-md- hidden-lg">

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-offset-2 col-sm-3 col-xs-5">
                            <ul class="list-unstyled list-btc">
                                <?php foreach($btc_list as $item): ?>
                                    <li><?php echo $item['btc']; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <div class="col-sm-2 col-xs-2">
                            <img  data-original="<?php echo get_template_directory_uri();?>/assets/images/timeline-bar-vertical.png" class="img-responsive horizontal-center img-timeline">

                            <div class="img-timeline-wrapper bg-center bg-contain lazy" data-original="<?php echo get_template_directory_uri();?>/assets/images/timeline-bar.png"></div>
                        </div>
                        <div class="col-sm-4 col-xs-5">
                            <ul class="list-unstyled list-btc list-bonus">
                                <?php foreach($bonus_list as $item): ?>
                                    <li>
                                        <p><?php echo $item['date']; ?></p>
                                        <p><?php echo $item['bonus']; ?></p>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
    
<?php
get_footer();
?>
