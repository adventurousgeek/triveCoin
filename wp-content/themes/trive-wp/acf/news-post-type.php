<?php
$save_format = 'url';
$library     = 'all';


$news_description                               = fields()->textarea('Description');
$news_image                                     = fields()->image('Image');
$news_link                                      = fields()->url( 'URL' );

//--Registering Metabox-----------------------------------------------------------
$metabox = ACFMetabox::instance();

$metabox->title( "News" )
    ->key( 'news_listing' )
    ->normal()
    ->location()
    ->post_type()
    ->value( 'news' )
;

$metabox->hide()->all()->permalink( false )->page_attributes( false );

$metabox->metabox()->fields( get_defined_vars() );

$metabox->register();
