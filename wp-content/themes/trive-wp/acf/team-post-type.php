<?php
$save_format = 'url';
$library     = 'all';

$tab_personal_information                       = fields()->tab( 'Team Member' );
$team_member_team                               = fields()->select('Team')
                                                    ->choice('Advisory' , 'advisory')
                                                    ->choice('Operations' , 'operations');
$team_member_designation                        = fields()->text('Designation');
$team_member_image                              = fields()->image('Speaker Image');
$team_member_linkedin                           = fields()->url( 'Linkedin Profile URL' );

//--Registering Metabox-----------------------------------------------------------
$metabox = ACFMetabox::instance();

$metabox->title( "Team Member Profile" )
    ->key( 'team_listing' )
    ->normal()
    ->location()
    ->post_type()
    ->value( 'team' )
;

$metabox->hide()->all()->permalink( false )->page_attributes( false );

$metabox->metabox()->fields( get_defined_vars() );

$metabox->register();
