<?php

$save_format = 'url';
$library     = 'all';


$tab_banner                                 = fields()->tab( 'Banner' );
$banner_heading                             = fields()->text( "Heading" );
$banner_sub_heading                         = fields()->text( "Sub Heading" );
$banner_button_text                         = fields()->text( 'Button Text' );
$banner_button_url                          = fields()->text( 'Button Url' );

$tab_main_countdown                         = fields()->tab(' Main Countdown');
$main_countdown_heading                     = fields()->text( "Heading" );
$main_countdown_button_text                 = fields()->text( 'Button Text' );
$main_countdown_button_url                  = fields()->text( 'Button Url' );

$tab_main_info                              = fields()->tab('Main Info');
$main_info_heading                          = fields()->text('Heading');
$main_info_content                          = fields()->wysiwyg( "Content" );

$tab_trive_plugin                           = fields()->tab('Plugin');
$trive_plugin_heading                       = fields()->text('Heading');
$trive_plugin_content                       = fields()->wysiwyg( "Content" );
$trive_plugin_link_text                     = fields()->text( 'Link Text' );
$trive_plugin_link_url                      = fields()->text( 'Link Url' );

$tab_trive_engine                           = fields()->tab('Engine');
$trive_engine_heading                       = fields()->text('Heading');
$trive_engine_content                       = fields()->wysiwyg( "Content" );
$trive_engine_link_text                     = fields()->text( 'Link Text' );
$trive_engine_link_url                      = fields()->text( 'Link Url' );

$tab_trive_participants                     = fields()->tab('Participants');
$trive_participants_heading                 = fields()->text('Heading');
$trive_participants_content                 = fields()->wysiwyg( "Content" );
$trive_participants_link_text               = fields()->text( 'Link Text' );
$trive_participants_link_url                = fields()->text( 'Link Url' );

$tab_initial_coin_offering                  = fields()->tab('ICO');
$initial_coin_offering_heading              = fields()->text('Heading');
$initial_coin_offering_content              = fields()->wysiwyg('Content');
$inital_coin_offering_countdown_heading     = fields()->text('Countdown Heading');
$inital_coin_offering_button_text           = fields()->text( 'Button Text' );
$inital_coin_offering_button_url            = fields()->text( 'Button Url' );


$tab_homepage_timeline_ico                  = fields()->tab('Timeline');

$homepage_timeline_ico_heading              = fields()->text('Heading');
$homepage_timeline_ico_btc_list             = fields()->repeater( 'BTC List' )
                                                ->min( 1 )
                                                ->max( 6 )
                                                ->block()
                                                ->button_label( 'Add another' )
                                                ->sub_field( 'btc', fields()->text( 'BTC' ) );

$homepage_timeline_ico_bonus_list           = fields()->repeater( 'Timeline List' )
                                                ->min( 1 )
                                                ->max( 5 )
                                                ->block()
                                                ->button_label( 'Add another' )
                                                ->sub_field( 'date', fields()->text( 'Date' ) )
                                                ->sub_field( 'bonus', fields()->text( 'Bonus' ) );



//--Registering Metabox-----------------------------------------------------------
$metabox = ACFMetabox::instance();

$metabox->title( "Additional Settings" )
        ->key( 'trive_homepage' )
        ->normal()
        ->location()
        ->page_template()
        ->value( 'page-templates/home.php' )
;

$metabox->hide()
        ->all()
        ->permalink( FALSE )
        ->page_attributes( FALSE )
;

$metabox->metabox()
        ->fields( get_defined_vars() )
;

$metabox->register();
