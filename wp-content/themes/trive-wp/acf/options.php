<?php

$save_format = 'url';
$library     = 'all';

$tab_cogs_behind                            = fields()->tab('Cogs Behind');
$cogs_behind_heading                        = fields()->text('Heading');
$cogs_behind_executive_heading              = fields()->text('Executive & Advisory Team');
$cogs_behind_operations_heading             = fields()->text('Opertaions Team');



$tab_footer                 = fields()->tab( 'Footer' );

$footer_about_heading       = fields()->text( 'Have questions - Heading' );
$footer_about_content       = fields()->textarea( 'Have questions - Content' );

$footer_resources_heading   = fields()->text('Resources - Heading');

$footer_copyright             = fields()->text('Copyright Text');


$tab_general        = fields()->tab( 'Social Platforms' );

$email_link       = fields()->text( 'Email' );
$linkedin_link      = fields()->url( 'Linkedin' );
$facebook_link      = fields()->url( 'Facebook' );
$twitter_link       = fields()->url( 'Twitter' );


//--Registering Metabox-----------------------------------------------------------
$metabox = ACFMetabox::instance();

$metabox->title( "Options" )
        ->key( 'options' )
        ->high()
;


$metabox->location( 'default' )
        ->options_page()
        ->value( 'options' )
;


$metabox->hide()
        ->all()
        ->page_attributes( FALSE )
;

$metabox->seamless()
        ->fields( get_defined_vars() )
;

$metabox->register();
