<?php

$save_format = 'url';
$library     = 'all';


$tab_ico_banner                                 = fields()->tab( 'Banner' );
$ico_banner_heading                             = fields()->text( "Heading" );
$ico_banner_date                                = fields()->text( "Date" );
$ico_banner_content                             = fields()->wysiwyg( 'Content' );

$ico_banner_list                                = fields()->repeater( 'Banner List' )
                                                    ->min( 1 )
                                                    ->max( 3 )
                                                    ->block()
                                                    ->button_label( 'Add another' )
                                                    ->sub_field( 'item', fields()->text( 'List Item' ) )
                                                    ->sub_field( 'url', fields()->text( 'URL' ) );

$ico_banner_countdown_heading                   = fields()->text('Countdown Heading');
$ico_banner_countdown_description               = fields()->text('Countdown Description');

$ico_banner_button_text                         = fields()->text( 'Button Text' );
$ico_banner_button_url                          = fields()->text( 'Button Url' );



$tab_ico_timeline_ico                           = fields()->tab('Timeline');
$ico_timeline_ico_heading                       = fields()->text('Heading');
$ico_timeline_ico_sub_heading                       = fields()->text('Sub Heading');
$ico_timeline_ico_btc_list                      = fields()->repeater( 'BTC List' )
                                                            ->min( 1 )
                                                            ->max( 6 )
                                                            ->block()
                                                            ->button_label( 'Add another' )
                                                            ->sub_field( 'rounds', fields()->text( 'Round' ) );

$ico_timeline_ico_bonus_list                    = fields()->repeater( 'Timeline List' )
                                                            ->min( 1 )
                                                            ->max( 5 )
                                                            ->block()
                                                            ->button_label( 'Add another' )
                                                            ->sub_field( 'date', fields()->text( 'Date' ) )
                                                            ->sub_field( 'bonus', fields()->text( 'Bonus' ) )
                                                            ->sub_field( 'amount', fields()->text( 'Amount' ) );


$tab_coin_offering                              = fields()->tab('Coin Offering');
$coin_offering_heading                          = fields()->text('Heading');
$coin_offering_list                             = fields()->repeater( 'Coin Offering List' )
                                                    ->min( 1 )
                                                    ->max( 6 )
                                                    ->block()
                                                    ->button_label( 'Add another' )
                                                    ->sub_field( 'heading', fields()->text( 'Heading' ) )
                                                    ->sub_field( 'value', fields()->text( 'Value' ) );

$tab_participate                                = fields()->tab('Participate');
$participate_heading                            = fields()->text('Heading');
$participate_sub_heading                        = fields()->text( 'Sub Heading' );
$participate_eth                                = fields()->text( 'ETH' );
$participate_paragraph                          = fields()->text( 'Content' );
$participate_guideline_text                     = fields()->text('Guideline Button Text');
$participate_guideline_link                     = fields()->text('Guideline Button Link');
$participate_scanqr_text                        = fields()->text('Scan QR Button Text');
$participate_scanqr_link                        = fields()->text('Scan QR Button Link');


$tab_platform_activity                          =  fields()->tab('Platform');
$platform_activity_heading                      = fields()->text('Heading');
$platform_activity_list                         = fields()->repeater( 'Stats' )
                                                    ->min( 1 )
                                                    ->max( 10 )
                                                    ->block()
                                                    ->button_label( 'Add another' )
                                                    ->sub_field( 'heading', fields()->text( 'Heading' ) )
                                                    ->sub_field( 'value', fields()->text( 'Value' ) );

$tab_roadmap                                    = fields()->tab('Roadmap');
$roadmap_heading                                = fields()->text('Heading');
$roadmap_list                                   = fields()->repeater( 'List' )
                                                    ->min( 1 )
                                                    ->max( 7 )
                                                    ->block()
                                                    ->button_label( 'Add another' )
                                                    ->sub_field( 'date', fields()->text( 'Date' ) )
                                                    ->sub_field( 'description', fields()->text( 'Description' ) )
                                                    ->sub_field( 'image', fields()->image('Image'));


$tab_ico_initial_coin_offering                  = fields()->tab('ICO');
$ico_initial_coin_offering_heading              = fields()->text('Heading');
$ico_initial_coin_offering_content              = fields()->wysiwyg('Content');
$ico_inital_coin_offering_countdown_heading     = fields()->text('Countdown Heading');
$ico_inital_coin_offering_button_text           = fields()->text( 'Button Text' );
$ico_inital_coin_offering_button_url            = fields()->text( 'Button Url' );

$tab_faqs                                       = fields()->tab('Faqs');
$faqs_heading                                   = fields()->text('Heading');
$faqs_sub_heading                               = fields()->text('Sub Heading');
$faqs_list                                      = fields()->repeater( 'Faq List' )
                                                    ->min( 1 )
                                                    ->block()
                                                    ->button_label( 'Add another' )
                                                    ->sub_field( 'question', fields()->text( 'Question' ) )
                                                    ->sub_field( 'answer', fields()->wysiwyg( 'Answer' ) );
$get_news_heading                               = fields()->text('Get News');
//--Registering Metabox-----------------------------------------------------------
$metabox = ACFMetabox::instance();

$metabox->title( "Additional Settings" )
        ->key( 'trive_ico' )
        ->normal()
        ->location()
        ->page_template()
        ->value( 'page-templates/ico.php' )
;

$metabox->hide()
        ->all()
        ->permalink( FALSE )
        ->page_attributes( FALSE )
;

$metabox->metabox()
        ->fields( get_defined_vars() )
;

$metabox->register();
