<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="favicon.ico">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri();?>/style.css">
    <?php wp_head(); ?>
</head>
<body>
<?php require_once get_template_directory() . "/inc/svg-icons.php"; ?>

<header class="site-header">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container header-container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url(); ?>">
                    <img data-original="<?php echo get_template_directory_uri();?>/assets/images/logo.png" class="img-responsive">
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">

                    <?php
                    $defaults = array(
                        'theme_location'  => 'main-menu',
                        'depth'           => 2,
                        'menu'            => '',
                        'container'       => false,
                        'container_id'    => '',
                        'menu_id'         => '',
                        'echo'            => true,
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul class="nav navbar-nav navbar-right">%3$s</ul>',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker()

                    );
                    wp_nav_menu( $defaults);
                    ?>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
</header>

