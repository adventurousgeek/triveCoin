<?php

$mu_plugins = array(
    'advanced-custom-fields-pro/acf.php',
    'disable-comments/disable-comments.php',
    'simple-custom-post-order/simple-custom-post-order.php',
    'contact-form-7/wp-contact-form-7.php',
    'autoptimize/autoptimize.php',
    //'wordpress-seo/wp-seo.php', Enable For SEO
);


foreach ( $mu_plugins as $plugin ) {
    require_once __DIR__ . '/' . $plugin;
}